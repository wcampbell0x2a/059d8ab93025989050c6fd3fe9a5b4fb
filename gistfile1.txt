### Keybase proof

I hereby claim:

  * I am wcampbell0x2a on github.
  * I am wcampbell (https://keybase.io/wcampbell) on keybase.
  * I have a public key whose fingerprint is A0B3 015B F635 563F A570  59D0 A040 F4DA FC68 FFE8

To claim this, I am signing this object:

```json
{
  "body": {
    "key": {
      "eldest_kid": "01202f55749f4355245232fc3b3db7f6bf8d59541d3b6c397d9ba19b5eb4c80ee9d70a",
      "fingerprint": "a0b3015bf635563fa57059d0a040f4dafc68ffe8",
      "host": "keybase.io",
      "key_id": "a040f4dafc68ffe8",
      "kid": "010134a86b458d39ffc6f341e7c9100a60f826e2b7518ef09a6395016a892ccf97620a",
      "uid": "e3f6917be2bed03572a57df89c8fd119",
      "username": "wcampbell"
    },
    "merkle_root": {
      "ctime": 1578529647,
      "hash_meta": "e394d5d794982198903435302c30de08fb25bca43d12ff7ac36ad78c7b363189",
      "seqno": 14146860
    },
    "revoke": {
      "sig_ids": [
        "a5cdd93096c4114e4b1a02f0beab7e01725dc9d975d0f859e615a136cfddb5080f"
      ]
    },
    "service": {
      "name": "github",
      "username": "wcampbell0x2a"
    },
    "type": "web_service_binding",
    "version": 1
  },
  "ctime": 1578529681,
  "expire_in": 157680000,
  "prev": "27e52d30678a8753a95fa29b0f12022129a368befdb06940e9734efd84fa4981",
  "seqno": 13,
  "tag": "signature"
}
```

with the key [A0B3 015B F635 563F A570  59D0 A040 F4DA FC68 FFE8](https://keybase.io/wcampbell), yielding the signature:

```
-----BEGIN PGP MESSAGE-----
Version: Keybase OpenPGP v2.1.3
Comment: https://keybase.io/crypto

yMP2AnicdZN9TJVVHMdBEAXyvdQ1NXxsLpDsPM95zvM8h5qTAE0HKExzvl7Oec45
3Cfg3uu9FwIJdVrOoDZTG5PluL6QU3zBSDZTU8jcjGG5bKKpS8NSSTcVs5suOvdO
t9bq+efsnH1/39/393nOOTkkLiYpNjS97+Jjd/+N2M6O8vKYZaMDH1Ur1MuqlIxq
pYRHF17KeCDoKnGYkqEAVQOaQMjUsdAhQpqONKgJG1LIqCkMKiyGMNJVBqlhQ2wy
TImKKeJUty3AOWYmIEq6IhxPMff7/I4nKG0JoBCoiApDehpQEGQChBkgQAdCZ0TY
hiUEt2Sh2xuIVMhwlAT4VMcrz+TGFY33H/qnuYEKdWIZVEcWg1hIhYC6yk0bqwAQ
AwhLM7hGTaRaXABMDIgRUA1iYc22BTYNLZq7PGrHoTCwalJZwBmAyNRkYiYsbFuC
qSqOCAPc7yFlXKrftkmZj/LSUqUmXSnj/pJS7vJ7vcEIXTvoRDQqMi2kYUM35YQk
4HaV8SCJNsI6Q8zEOrY0FVsYQIkdAs2GgHFgCaohahMdMlUTwiQ2NAgzLduk0ICq
FQkS4Ms9XtlBV3XDMoCM4OcV3hIe6R5wiiW4gJKxWCHIZgxDgA1bV1Wd61Ql8lcD
ygk1OVBNDTEbM2wiJlkhzA0VERUatmCMImABoSytiXTzVzh21PzJ9MVO0F1O/4cI
qNRIhEqwyhc959T1xMFFHQ+Tl0QWVnB/wPF65AxS+S9glpqu8Eqf4+cuxxM9Niwg
v3TFJ8eUlprJkcYgMEyLWCaCBCNBNEyBiFxlTdUwgYZFuWAUGFgHHJtQlztLF0RC
V/9BEMqcpFh6SmweEiz3c6Wmo31JfExsUkzCwAGR1xOTlDj86Zs6dnxY/7LUcYd/
a7r9qC2m78DQ16ataZgx7DOz7vzga/FVK3tGb9m9Mzi+7VFv/dhQ562UV1fM2TBr
65CN4cLE2cGE1NVbJqhp+26nXp373oI961d4BxXt637clJXRjFobF40rOpd8b3LN
+a74/L4p7sb8hafynO0nKzo9pye1b2+ZeX/l5dj5f6kpjXd8leNrybTdXfXTzxd/
vjz1eNqe5VPabhaMzQVncg/O+GTSJU/37tN927LXzWijp7rit4a6e5ou2S/WVe2f
s9SbcDBzVSg589MdRmF6T8G8X75j5clJPyYdGNmaMnfTMvpO3c7L5oO1t1a0vTH1
WTBx0pAPrPpr7aFG3yvr3uq729sZ2nElp+WbOVkT035KHky/umTcG9kTTFtdt7l5
6LErfH9LQ71T11J39uGN+c+E51+fvD/ui7ykwv5DiS+vbT2ypAVm03cPfX828OC5
oznNtQ0B/9zOy1f7z7kGpM2m4dCR+q76xR+vOZG49+j9w4vH5P3wa87sP4YfzI8b
1P76lymVudtcOe6WcydnFvQ+P74x4e4of/7KRcUbDkzMvjjmVM4FX9HGsUUvfZ3p
PxHo+DC3oXFCor6rdt+8VpC5l/0+4syCpiUX1m9e2LvXyuqmD2e59c3+7NV37mV8
u+vP8Cqe2pxwc1PT9aK4wJ6lTXnpneHUKesKq0eEnayOgvfDP9OBC0dlz6p+84VV
nglX/wZYrgGg
=rbYt
-----END PGP MESSAGE-----

```

And finally, I am proving ownership of the github account by posting this as a gist.

### My publicly-auditable identity:

https://keybase.io/wcampbell

### From the command line:

Consider the [keybase command line program](https://keybase.io/download).

```bash
# look me up
keybase id wcampbell
```
